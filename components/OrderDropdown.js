import React, { useState, useEffect, useRef } from "react";

const OrderDropdown = ({ games, setGames }) => {
  const [currentOrder, setCurrentOrder] = useState("Relevance");

  const [show, setShow] = useState(false);
  const refOne = useRef(null);

  const sortByName = () => {
    const sortedByName = games.sort((a, b) => {
      return a.name.localeCompare(b.name);
    });
    setGames(() => [...sortedByName]);
  };

  const sortByRating = () => {
    const sortedByRating = games.sort((a, b) => {
      return b.rating - a.rating;
    });
    setGames(() => [...sortedByRating]);
  };

  const sortByDate = () => {
    const sortedByDate = games.sort((a, b) => {
      return b.released.localeCompare(a.released);
    });
    setGames(() => [...sortedByDate]);
  };

  const handleClickOutside = (e) => {
    if (refOne.current !== null) {
      if (refOne.current.contains(e.target)) {
        setShow(true);
      } else {
        setShow(false);
      }
    }
  };

  useEffect(() => {
    document.addEventListener("click", handleClickOutside, true);
  });

  return (
    <React.Fragment>
      <div className="container p-10 mx-auto  ">
        <div ref={refOne} className="relative inline-block text-left ">
          <div>
            <button
              onClick={() => setShow(true)}
              type="button"
              className="inline-flex w-full justify-center rounded-md border border-gray-300 bg-slate-700 text-white px-4 py-2 text-sm font-medium shadow-sm focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 focus:ring-offset-gray-100"
              id="menu-button"
              aria-expanded="true"
              aria-haspopup="true"
            >
              Order by {currentOrder}
              <svg
                className="-mr-1 ml-2 h-5 w-5"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
                fill="currentColor"
                aria-hidden="true"
              >
                <path
                  fillRule="evenodd"
                  d="M5.23 7.21a.75.75 0 011.06.02L10 11.168l3.71-3.938a.75.75 0 111.08 1.04l-4.25 4.5a.75.75 0 01-1.08 0l-4.25-4.5a.75.75 0 01.02-1.06z"
                  clipRule="evenodd"
                />
              </svg>
            </button>
          </div>
          {show ? (
            <div
              className="absolute right-50 z-10 mt-2 w-56 origin-top-right rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none"
              role="menu"
              aria-orientation="vertical"
              aria-labelledby="menu-button"
              tabIndex={-1}
            >
              <div className="py-1" role="none">
                <a
                  onClick={() => {
                    setCurrentOrder("Name");
                    sortByName();
                    setShow(false);
                  }}
                  className="text-gray-700 block px-4 py-2 text-sm cursor-pointer"
                  role="menuitem"
                  tabIndex={-1}
                  id="menu-item-0"
                >
                  Name
                </a>
                <a
                  onClick={() => {
                    setCurrentOrder("Rating");
                    sortByRating();
                    setShow(false);
                  }}
                  className="text-gray-700 block px-4 py-2 text-sm cursor-pointer"
                  role="menuitem"
                  tabIndex={-1}
                  id="menu-item-1"
                >
                  Rating
                </a>
                <a
                  onClick={() => {
                    setCurrentOrder("Date");
                    sortByDate();
                    setShow(false);
                  }}
                  className="text-gray-700 block px-4 py-2 text-sm cursor-pointer"
                  role="menuitem"
                  tabIndex={-1}
                  id="menu-item-2"
                >
                  Date
                </a>
              </div>
            </div>
          ) : null}
        </div>
      </div>
    </React.Fragment>
  );
};

export default OrderDropdown;
