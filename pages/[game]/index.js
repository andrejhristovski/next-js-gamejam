import React from "react";
import { useSelector } from "react-redux";
import Link from "next/link";
import { useRouter } from "next/router";

function GameView() {
  const state = useSelector((state) => state.user);
  let isLoggedIn = state.user.username ? true : false;

  const router = useRouter();
  let game = router.query;

  const { name, background_image, released, rating, genres } = game;

  return (
    <div className="flex min-h-full items-stretch justify-center text-center md:items-center md:px-2 lg:px-4">
      <div className="flex w-full text-left text-base  md:my-8 md:max-w-2xl md:px-4 lg:max-w-4xl">
        <div className="relative flex w-full items-center overflow-hidden bg-white px-4 pt-14 pb-8 sm:px-6 sm:pt-8 md:p-6 lg:p-8">
          <div className="grid w-full grid-cols-1 items-start gap-y-8 gap-x-6 sm:grid-cols-12 lg:gap-x-8">
            <div className="overflow-hidden rounded-lg bg-gray-100 sm:col-span-4 lg:col-span-5">
              <img
                style={{ height: 400, width: "100%" }}
                src={background_image}
                alt="Two each of gray, white, and black shirts arranged on table."
                className="object-cover object-center"
              />
            </div>
            <div className="sm:col-span-8 lg:col-span-7">
              <h2 className="text-2xl font-bold text-gray-900 sm:pr-12">
                {name}
              </h2>

              <section className="mt-2">
                <div className="mt-6">
                  <div className="flex items-center">
                    <div className="flex items-center">
                      <p>Rating : {rating}</p>
                    </div>
                  </div>
                </div>
              </section>
              <section className="mt-10 flex flex-col items justify">
                <div>
                  <h4 className="text-sm font-medium text-gray-900">
                    Genres :{" "}
                    {genres.map((e, key) => (
                      <span key={key}>{e.name},</span>
                    ))}
                  </h4>
                </div>
                <div className="mt-10">
                  <div className="flex items-center justify-between">
                    <h4 className="text-sm font-medium text-gray-900">
                      Released : {released}
                    </h4>
                  </div>
                </div>
                {!isLoggedIn ? (
                  <Link href={`/login`}>
                    <button
                      type="submit"
                      className="mt-6 flex w-full items-center justify-center rounded-md border border-transparent bg-indigo-600 py-3 px-8 text-base font-medium text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
                    >
                      Log in to play the game
                    </button>
                  </Link>
                ) : null}
              </section>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default GameView;
