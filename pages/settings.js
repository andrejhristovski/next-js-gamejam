import React, { useState } from "react";

import Image from "next/image";
import { useRouter } from "next/router";

import { useDispatch } from "react-redux";
import { changeUsername } from "../redux/user";

import logo from "../assets/logo.png";

function Settings() {
  const [newUsername, setNewUsername] = useState("");

  const dispatch = useDispatch();
  const router = useRouter();

  let handleSubmit = (e) => {
    if (newUsername !== "") {
      e.preventDefault();
      router.push("/");
      dispatch(changeUsername(newUsername));
    }
  };
  return (
    <section className="bg-gray-50 dark:bg-gray-900">
      <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0">
        <p className="flex items-center mb-6 text-2xl font-semibold text-gray-900 dark:text-white">
          <Image src={logo} alt="Logo" width={50} height={50} />
          Gamejam
        </p>
        <div className="w-full bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">
          <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
            <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
              Change Username
            </h1>
            <form className="space-y-4 md:space-y-6" action="#">
              <div>
                <label
                  htmlFor="password"
                  className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                >
                  New Username
                </label>
                <input
                  onChange={(e) => setNewUsername(e.target.value)}
                  type="text"
                  name="text"
                  id="text"
                  placeholder="Billy Doe"
                  className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-slate-600 focus:border-slate-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  required
                />
              </div>
              <button
                onClick={(e) => {
                  handleSubmit(e);
                }}
                type="submit"
                className="w-full text-white bg-slate-600 hover:bg-slate-700 focus:ring-4 focus:outline-none focus:ring-slate-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-slate-600 dark:hover:bg-slate-700 dark:focus:ring-slate-800"
              >
                Change Username
              </button>
            </form>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Settings;
