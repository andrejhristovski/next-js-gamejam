import { createSlice } from "@reduxjs/toolkit";

export const UserSlice = createSlice({
  name: "user",
  initialState: {
    user: {},
  },
  reducers: {
    setUser: (state, action) => {
      state.user = action.payload;
    },
    changeUsername: (state, action) => {
      state.user.username = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setUser, changeUsername } = UserSlice.actions;

export default UserSlice.reducer;
